import java.sql.*;


public class LandwirtschaftSQL {

	private static Connection c;
	
	public static void main(String[] args) 
	{
		try{
			Class.forName("org.sqlite.JDBC");
			c=DriverManager.getConnection("jdbc:sqlite:landwirtschaft.db");
			
			Cleartable("Tiere");
			Cleartable("Landwirtschaft");
			Cleartable("Stall");
			Cleartable("StallTiere");
			Cleartable("BesitzerTiere");
			Cleartable("Besitzer");
			createTableTiere();
			createTablelandwirtschaft();
			//anzahlTiere();
			createTableStall();
			createTableStalltiere();
			createTableBesitzerTiere();
			createTableBesitzer();
			insertTiere("KUH", 145, "Milka", 16);
			insertLandwirtschaft("Herbert Schuh", 3254, 6150, "Steinach", "Friedrichsstra�e");
			insertStall("Kuhstall", 60, 2011, "3000m�", "KuhzuchtSchuh", 3254);
			insertStallTiere("Kuhstall", "Milka");
			insertBesitzer("Schuh", "Herbert", 6150, "Steinach", "Friedrichsstra�e", 3254, "Kuhstall");
			insertBesitzerTiere("Schuh", "Herbert", "Kuhstall");
			c.close();
			
			
		}
		catch(Exception e){
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);	
		}

	}
	public static void Cleartable(String clearSql) throws SQLException{
		Statement stmt = c.createStatement();
		String drop = "drop table "+clearSql;
		stmt.executeUpdate(drop);
		stmt.close();
	}
	
    public static void createTableTiere() throws SQLException{
    	
    	Statement stmt= c.createStatement();
    	
    	String tiertableSQL = "CREATE TABLE IF NOT EXISTS Tiere("+"Name TEXT Primary Key," 
                + "Art TEXT,"+" Alt INT NOT NULL," 
                +"Gewicht INT NOT NULL);";
    	

    	System.out.println("created Tiere succesfully");
    	stmt.execute(tiertableSQL);
    	stmt.close();
    	
	}
    public static void insertTiere(String Art, double Gewicht, String Name, int Alt) throws SQLException
   	{
   		Statement stmt = c.createStatement();
   		String sql = "INSERT INTO Tiere (Art, Gewicht, Name, ALt) " + 
                "VALUES (\""+Art+"\", "+Gewicht+", \""+Name+"\", "+Alt+");"; 
   		System.out.println(sql);
   		stmt.executeUpdate(sql);
   		System.out.println("inserted successfully");
   		stmt.close();
   	}
    
    
    
 public static void createTablelandwirtschaft() throws SQLException{
    	
    	Statement stmt= c.createStatement();

    	String tableLandwirtschaft= "Create table if not exists Landwirtschaft"
    	        +"(Steuernummer INT NOT NULL,"
                +"Name TEXT," 
                +"PLZ INT,"+" Ort TEXT,"+" Stra�e TEXT,"
                +"PRIMARY KEY(Steuernummer, Name));";
    	

    	System.out.println("created Landwirtschaft succesfully");
    	stmt.execute(tableLandwirtschaft);
    	stmt.close();
     }
 public static void insertLandwirtschaft(String Name, int Steuernummer, int PLZ, String Ort, String Stra�e) throws SQLException
	{
		Statement stmt = c.createStatement();
		String sql = "INSERT INTO Landwirtschaft(Name, Steuernummer, PLZ, Ort, Stra�e)" +
             "VALUES (\""+Name+"\","+Steuernummer+","+PLZ+",\""+Ort+",\""+Stra�e+"\");"; 
		System.out.println(sql);
		stmt.executeUpdate(sql);
		System.out.println("inserted successfully");
		stmt.close();
	}
 
 
 
 public static void createTableStall() throws SQLException{
 	
 	Statement stmt= c.createStatement();

 	String tableStall="create table if not exists Stall"
 			+ "( Bezeichnung TEXT,"
 			+ " Pl�tze INT," 
            +"Baujahr INT NOT NULL,"
            +"Fl�che INT NOT NULL,"
            +"LName TEXT,"
            + "LSteuernummer INT NOT NULL,"
            +"PRIMARY KEY(Bezeichnung, LName, LSteuernummer),"
            +"FOREIGN KEY(LName, LSteuernummer) references Landwirtschaft(Name, Steuernummer));";


 	System.out.println("created table Stall succesfully");
 	stmt.execute(tableStall);
 	stmt.close();
  }
 public static void insertStall(String Bezeichnung, int PL�tze, int Baujahr, String Fl�che, String LName,int LSteuernummer) throws SQLException
	{
		Statement stmt = c.createStatement();
		String sql = "INSERT INTO Stall(Bezeichnung, PL�tze, Baujahr, Fl�che, LName, LSteuernummer)" +
             "VALUES (\""+Bezeichnung+"\","+PL�tze+","+Baujahr+",\""+Fl�che+"\",\""+LName+"\","+LSteuernummer+");"; 
		System.out.println(sql);
		stmt.executeUpdate(sql);
		System.out.println("inserted successfully");
		stmt.close();
	}
 
 
 
 public static void createTableStalltiere() throws SQLException{
	 	
	 	Statement stmt= c.createStatement();

	 	String tableStalltiere="create table if not exists StallTiere("
	 			+ "SBezeichnung TEXT,"
	 			+ " TName TEXT,"
	 			+ "PRIMARY KEY(SBezeichnung, TName),"
	 			+ "FOREIGN KEY(SBezeichnung) references Stall(Bezeichnung),"
                + "FOREIGN KEY(TName) references Tiere(Name));";


	 	System.out.println("created StallTiere succesfully");
	 	stmt.execute(tableStalltiere);
	 	stmt.close();
	  }
 public static void insertStallTiere(String SBezeichnung, String TName) throws SQLException
	{
		Statement stmt = c.createStatement();
		String sql = "INSERT INTO StallTiere(SBezeichnung, TName)" +
             "VALUES (\""+SBezeichnung+"\",\""+TName+"\");"; 
		System.out.println(sql);
		stmt.executeUpdate(sql);
		System.out.println("inserted successfully");
		stmt.close();
	}
 
 

 public static void createTableBesitzer() throws SQLException{
	 	
	 	Statement stmt= c.createStatement();

	 	String tableBesitzer="create table if not exists Besitzer("
	 			+ "Nachname TEXT,"
	 			+ " Vorname TEXT, "
	 			+ "PLZ INT NOT NULL,"
	 			+ "Ort TEXT,"
	 			+ "Stra�e TEXT,"
                +"LSteuernummer INT NOT NUll,"
                + "LName TEXT,"
                +"PRIMARY KEY(Nachname, Vorname, LSteuernummer, LName),"
                +"FOREIGN KEY(LSteuernummer, LName) references Landwirtschaft(Steuernummer, Name));";

	 	System.out.println("created Besitzer succesfully");
	 	stmt.execute(tableBesitzer);
	 	stmt.close();
	  }
 public static void insertBesitzer(String Nachname, String Vorname, int PLZ, String Ort, String Stra�e, int LSteuernummer, String LName) throws SQLException
	{
		Statement stmt = c.createStatement();
		String sql = "INSERT INTO StallTiere(Nachname, Vorname, PLZ, Ort, Stra�e, LSteuernummer, LName)" +
             "VALUES (\""+Nachname+"\",\""+Vorname+"\", "+PLZ+", \""+Ort+"\", \""+Stra�e+"\", "+LSteuernummer+", \""+LName+"\");"; 
		System.out.println(sql);
		stmt.executeUpdate(sql);
		System.out.println("inserted successfully");
		stmt.close();
	}
 
 
 
 public static void createTableBesitzerTiere() throws SQLException{
	 	
	 	Statement stmt= c.createStatement();

	 	String tableBesitzerTiere="create table if not exists BesitzerTiere("
	 			+ "BNachname TEXT,"
	 			+ " BVorname TEXT,"
	 			+ " TName TEXT,"
	 			+ "PRIMARY KEY(BNachname, BVorname, TName),"
             +"FOREIGN KEY(BNachname, BVorname) references Besitzer(Nachname, BVorname),"
             +"FOREIGN KEY(TName) references Tiere(Name));";


	 	System.out.println("created BesitzerTiere succesfully");
	 	stmt.execute(tableBesitzerTiere);
	 	stmt.close();
	  }
 public static void insertBesitzerTiere(String BNachname, String BVorname, String TName) throws SQLException
	{
		Statement stmt = c.createStatement();
		String sql = "INSERT INTO StallTiere(BNachname, BVorname, TName)" +
             "VALUES (\""+BNachname+"\", \""+BVorname+"\", \""+TName+"\");"; 
		System.out.println(sql);
		stmt.executeUpdate(sql);
		System.out.println("inserted successfully");
		stmt.close();
	}

 
 
 public static int anzahlTiere() throws SQLException{

		Statement stmt = c.createStatement();

		ResultSet rs = stmt.executeQuery( "SELECT Alt FROM Tiere;" );
		int zaehler=0;
		while ( rs.next() ) {
			int Alt = rs.getInt("Alt");
			zaehler++;
			
		}
		System.out.println(zaehler);

		rs.close();
		stmt.close();

		return zaehler;
	}
   }




