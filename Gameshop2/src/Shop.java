import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Shop {

	public static void main(String[] args) {
		boolean alleloeschen=false;
		Connection c= null;
		Statement stmt = null;
		
		try{
			Class.forName("org.sqlite.JDBC");
			c=DriverManager.getConnection("jdbc:sqlite:ErsteHausuebung.db");
			System.out.println("opened database successfully");
			stmt = c.createStatement();
			
			if(alleloeschen==true){
				stmt.executeUpdate("drop table Kunde");
				stmt.executeUpdate("drop table Shop");
				stmt.executeUpdate("drop table Kundenbeziehung");
				stmt.executeUpdate("drop table Spiel");
				stmt.executeUpdate("drop table Bestellung");
				stmt.executeUpdate("drop table Mitarbeiter");
				stmt.executeUpdate("drop table Unternhehmen");
			}
			

		      
		      String sql = "CREATE TABLE IF NOT EXISTS Kunde " +
		                   "(Namen TEXT PRIMARY KEY     NOT NULL," +
		                   " Adresse   TEXT    NOT NULL)"; 
		      stmt.executeUpdate(sql);
		      
		      sql = "CREATE TABLE IF NOT EXISTS Kundenspielbeziehung " +
		                   "(NamenK TEXT  NOT NULL," +
		                   " Datum   INT    NOT NULL," +
		                   "NamenS TEXT NOT NULL,"
		                   + "Primary Key(NamenK, NamenS), "
		                   + "Foreign Key(NamenK) references Kunde(Namen),"
		                   + "Foreign Key(NamenS) references Spiel(Namen))"; 
		      stmt.executeUpdate(sql);
		      
		      sql = "CREATE TABLE IF NOT EXISTS Spiel " +
		                   "(Namen TEXT PRIMARY KEY     NOT NULL," +
		                   " Preis   INT    NOT NULL)"; 
		      stmt.executeUpdate(sql);
		   
		      sql = "CREATE TABLE IF NOT EXISTS Shop " +
		                   "(Unternehmensnamen TEXT PRIMARY KEY NOT NULL," +
		                   " Öffnungszeiten INT    NOT NULL,"
		                   + "PLZ INT NOT NULL)"; 
		      stmt.executeUpdate(sql);
		      sql = "CREATE TABLE IF NOT EXISTS Bestellung " +
	                   "(KundenNamen TEXT PRIMARY KEY     NOT NULL," +
	                   " BestellNr INT Primary Key   NOT NULL," + " Menge INT    NOT NULL,"
	                   		+ "Foreign Key(Kundenname) references Kunde(Namen))";  
            stmt.executeUpdate(sql);
            sql = "CREATE TABLE IF NOT EXISTS Mitarbeiter " +
                   "(Namen TEXT PRIMARY KEY     NOT NULL," +
                   " Gehalt INT    NOT NULL,"+ " Arbeitsort TEXT    NOT NULL, "
                   		+ "Foreign Key(Arbeitsort) references Shop(Unternehmensname))"; 
           stmt.executeUpdate(sql);
           sql = "CREATE TABLE IF NOT EXISTS Unternhemen " +
                  "(Frimennamen TEXT PRIMARY KEY     NOT NULL)"; 
           stmt.executeUpdate(sql);
		      
		    stmt.close();
		    c.close();
	    }
		catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
	    }
		    System.out.println("Table created successfully");
	}

}
