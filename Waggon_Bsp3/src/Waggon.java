import java.sql.*;
import java.util.Scanner;


public class Waggon{

	
	private static Connection c;
	
	
	public static void main(String[] args) {
		try{
			Class.forName("org.sqlite.JDBC");
			c=DriverManager.getConnection("jdbc:sqlite:waggon_Bsp3.db");
			
			
			/*Cleartable("waggon");
			Cleartable("Sitzplatz");
			Cleartable("Kunde");*/
			createtableWaggon();
			createtableSitzplatz();
			createtableKunde();
			insertWaggon(7, 1);
			insertWaggon(3, 2);
			insertSitzplatz(43, 7);
			insertSitzplatz(24, 3);
			insertKunde(243, "berni", 24);
			insertKunde(72, "hans", 43); 
			
			/*Waggon selects = new Waggon();
	        selects.selectAll();*/
	
	        
	        System.out.println("Geben Sie bitte einen Namen ein: ");
	        Scanner sc = new Scanner(System.in);
	        // int i = sc.nextInt();
	        String username = sc.nextLine();
	        
	        zeigeNameZuKlasse(username);
	        
			c.close();
			
			
		}
		catch(Exception e){
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);	
		}

	}
	public static void Cleartable(String clearSql) throws SQLException{
		Statement stmt = c.createStatement();
		String drop = "drop table "+clearSql;
		stmt.executeUpdate(drop);
		stmt.close();
	}
	
	
 public static void createtableWaggon() throws SQLException{
    	
    	Statement stmt= c.createStatement();
    	
    	String waggonSQL = "CREATE TABLE IF NOT EXISTS waggon("+"waggonID int Primary Key," 
                + "klasse int);";
    	

    	System.out.println("created waggon succesfully");
    	stmt.execute(waggonSQL);
    	stmt.close();	
	}
    
    
    public static void insertWaggon(int waggonID, int klasse) throws SQLException
   	{
   		Statement stmt = c.createStatement();
   		String sql = "INSERT INTO Waggon (waggonID, klasse) " + 
                "VALUES ("+waggonID +", "+ klasse+");"; 
   		System.out.println(sql);
   		stmt.executeUpdate(sql);
   		System.out.println("inserted successfully");
   		stmt.close();
   	}
    
	
 public static void createtableSitzplatz() throws SQLException{
    	
    	Statement stmt= c.createStatement();
    	
    	String SitzplatzSQL = "CREATE TABLE IF NOT EXISTS Sitzplatz("+"sitzplatz int,"
    			+ " waggonID2 int references waggon(waggonID),"
    			+ " primary key(sitzplatz, waggonID2));";
    	

    	System.out.println("created Sitzplatz succesfully");
    	stmt.execute(SitzplatzSQL);
    	stmt.close();	
	}
    
    
    public static void insertSitzplatz(int sitzplatz, int waggonID2) throws SQLException
   	{
   		Statement stmt = c.createStatement();
   		String sql = "INSERT INTO Sitzplatz (waggonID, klasse) " + 
                "VALUES ("+sitzplatz +", "+ waggonID2+");"; 
   		System.out.println(sql);
   		stmt.executeUpdate(sql);
   		System.out.println("inserted successfully");
   		stmt.close();
   	}
	
	
public static void createtableKunde() throws SQLException{
    	
    	Statement stmt= c.createStatement();
    	
    	String KundeSQL = "CREATE TABLE IF NOT EXISTS Kunde("+"kundenID INT NOT NULL," 
                + "name TEXT,"+" Sitzplatz2 INT NOT NULL,"
                + "primary key(kundenID, Sitzplatz2),"
                + "Foreign Key Sitzplatz2 references Sitzplatz(sitzplatz));";
    	

    	System.out.println("created Kunde succesfully");
    	stmt.execute(KundeSQL);
    	stmt.close();	
	}
    
    
    public static void insertKunde(int kundenID, String Name, int Sitzplatz2) throws SQLException
   	{
   		Statement stmt = c.createStatement();
   		String sql = "INSERT INTO Kunde(kundenID, Name, Sitzplatz2) " + 
                "VALUES ("+kundenID+", \""+Name+"\", "+Sitzplatz2+");"; 
   		System.out.println(sql);
   		stmt.executeUpdate(sql);
   		System.out.println("inserted successfully");
   		stmt.close();
   	}
    
    //SELECTS
    
    /*
        private Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:C://sqlite/db/test.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
    
    public void selectAll(){
        String sql = "SELECT Kunde.name , Waggon.klasse from Kunde, Waggon,"
        		+ " Sitzplatz where Waggon.waggonID = Sitzplatz.waggonID2 and Kunde.sitzplatz2 = Sitzplatz.sitzplatz order by Kunde.name"; 
        		
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getInt("kundenID") +  " | " + 
                                   rs.getInt("klasse"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    } 
    */
    
    public static void zeigeNameZuKlasse(String name){
    	
    	String sgl = "select w.klassenbezeichnung from kunden k, "
    			+ "waggonklassen w, sitz2Waggon s where w.waggonID = s.waggonID "
    			+ "and k . sitzplatz = s.sitzplatzID and k.name = \""+name+";";
    	
    }
    
}


